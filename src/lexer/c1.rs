use logos::Logos;

#[derive(Logos, Debug, PartialEq)]
pub enum C1Token {
    #[token("bool")]
    KwBoolean,

    #[token("do")]
    KwDo,

    #[token("else")]
    KwElse,

    #[token("float")]
    KwFloat,

    #[token("for")]
    KwFor,

    #[token("if")]
    KwIf,

    #[token("int")]
    KwInt,

    #[token("printf")]
    KwPrintf,

    #[token("return")]
    KwReturn,

    #[token("void")]
    KwVoid,

    #[token("while")]
    KwWhile,

    #[token("+")]
    Plus,

    #[token("-")]
    Minus,

    #[token("*")]
    Asterisk,

    #[token("/")]
    Slash,

    #[token("=")]
    Assign,

    #[token("==")]
    Eq,

    #[token("!=")]
    Neq,

    #[token("<")]
    Lss,

    #[token(">")]
    Grt,

    #[token("<=")]
    Leq,

    #[token(">=")]
    Geq,

    #[token("&&")]
    And,

    #[token("||")]
    Or,

    #[token(",")]
    Comma,

    #[token(";")]
    Semicolon,

    #[token("(")]
    LParen,

    #[token(")")]
    RParen,

    #[token("{")]
    LBrace,

    #[token("}")]
    RBrace,

    #[regex(r"[0-9]+")]
    ConstInt,

    #[regex(r"([0-9]+\.[0-9]+|\.[0-9]+)([eE]([-+])?[0-9]+)?|[0-9]+[eE]([-+])?[0-9]+")]
    ConstFloat,

    #[regex(r"true|false")]
    ConstBoolean,

    #[regex(r#""[^\n"]*""#)]
    ConstString,

    #[regex(r"([a-zA-Z])+([0-9]|[a-zA-Z])*")]
    Id,

    #[regex(r#"/\*[^\*/]*\*/"#, logos::skip)]
    CComment,

    #[regex(r"//.*", logos::skip)]
    CppComment,

    // Logos requires one token variant to handle errors,
    // it can be named anything you wish.
    #[error]
    // We can also use this variant to define whitespace,
    // or any other matches we wish to skip.
    #[regex(r"[ \t\n\f\r]+", logos::skip)]
    Error,
}
