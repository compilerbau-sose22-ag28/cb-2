use logos::{Lexer, Logos, Source};
use std::fmt::{Display, Formatter};

/// Tuple struct for link URLs
#[derive(Debug, PartialEq)]
pub struct LinkUrl(String);

/// Implement Display for printing
impl Display for LinkUrl {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        self.0.fmt(f)
    }
}

/// Tuple struct for link texts
#[derive(Debug, PartialEq)]
pub struct LinkText(String);

/// Implement Display for printing
impl Display for LinkText {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        self.0.fmt(f)
    }
}

/// Token enum for capturing of link URLs and Texts
#[derive(Logos, Debug, PartialEq)]
pub enum URLToken {
    // Capture link definitions
    #[regex(r#"<a[^h]*href="[^"]*"[^>]*>[^<]*</a[ \r\n\t]*>"#, extract_link_info)]
    Link((LinkUrl, LinkText)),

    // Ignore all characters that do not belong to a link definition
    // ignore all other < TAGS >
    #[regex(r"<[^>]*>", logos::skip)]
    // ignore everything not in < TAGS >
    #[regex(r"[^<]*", logos::skip)]
    Ignored,

    // Catch any error
    #[error]
    Error,
}

/// Extracts the URL and text from a string that matched a Link token
fn extract_link_info(lex: &mut Lexer<URLToken>) -> (LinkUrl, LinkText) {
    let slice = lex.slice();

    // find start and end index of URL in slice
    let url_start = slice.find(r#"href=""#).unwrap() + 6;
    let url_end = url_start + slice[url_start..].find(r#"""#).unwrap();

    // find start and end index of TEXT in slice
    let text_start = slice.find(">").unwrap() + 1;
    let text_end = text_start + slice[text_start..].find("<").unwrap();

    (LinkUrl(slice[url_start..url_end].to_string()), LinkText(slice[text_start..text_end].to_string()))
}

